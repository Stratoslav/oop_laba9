﻿using OOP_Laba9.Organization;

public class Program
{
    public static void Main()
    {
        OrganizationCollection organizationCollection = new OrganizationCollection();

        organizationCollection.AddOrganization(new Organization { Name = "Org1", EmployeeCount = 100, Rating = 4 });
        organizationCollection.AddOrganization(new Organization { Name = "Org2", EmployeeCount = 50, Rating = 3 });
        organizationCollection.AddOrganization(new Organization { Name = "Org3", EmployeeCount = 200, Rating = 5 });

        foreach (Organization org in organizationCollection)
        {
            Console.WriteLine($"{org.Name}, EmployeeCount: {org.EmployeeCount}, Rating: {org.Rating}");
        }
    }
}