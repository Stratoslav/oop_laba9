﻿

namespace OOP_Laba9.Organization
{
    public class Organization : IComparable<Organization>
    {
        public string Name { get; set; }
        public int EmployeeCount { get; set; }
        public int Rating { get; set; }
        public int CompareTo(Organization other)
        {
          
            return this.EmployeeCount.CompareTo(other.EmployeeCount);
        }
    }
}
