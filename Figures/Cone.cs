﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba9.Figures
{
    public class Cone : TrigonometricFigure
    {
        private double radius;
        private double height;

        public Cone(double radius, double height)
        {
            this.radius = radius;
            this.height = height;
        }

        public override double GetVolume()
        {
            return (1.0 / 3.0) * Math.PI * Math.Pow(radius, 2) * height;
        }
    }
}
