﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba9.Figures
{
    public class Sphere : TrigonometricFigure
    {
        private double radius;

        public Sphere(double radius)
        {
            this.radius = radius;
        }

        public override double GetVolume()
        {
            return (4.0 / 3.0) * Math.PI * Math.Pow(radius, 3);
        }
    }
}
