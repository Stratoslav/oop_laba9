﻿

namespace OOP_Laba9.Figures
{

     interface ITrigonometricFigure
    {
        double GetVolume();
    }

    public abstract class TrigonometricFigure : ITrigonometricFigure
    {
        public abstract double GetVolume();
    }
}
